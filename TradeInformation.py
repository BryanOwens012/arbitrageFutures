
class Commission:
   def __init__(self):
         self._commission = {'jm': {'rate': 1.2, 'value': 0}, 'j': {'rate': 1.2, 'value': 0}, 'i': {'rate': 1.2, 'value': 0},
                              'pp': {'rate': 1.2, 'value': 0},
                              'jd': {'rate': 1.5, 'value': 0}, 'ru': {'rate': 0.45, 'value': 0}, 'rb': {'rate': 1, 'value': 0},
                              'hc': {'rate': 1, 'value': 0}, 'bu': {'rate': 1, 'value': 0}, 'cu': {'rate': 0.5, 'value': 0},
                              'ag': {'rate': 0.5, 'value': 0},
                              'pb': {'rate': 0.4, 'value': 0}, 'cs': {'rate': 0, 'value': 1.5}, 'm': {'rate': 0, 'value': 1.5},
                              'a': {'rate': 0, 'value': 2}, 'c': {'rate': 0, 'value': 1.2}, 'y': {'rate': 0, 'value': 2.5},
                              'l': {'rate': 0, 'value': 2},
                              'p': {'rate': 0, 'value': 2.5}, 'ni': {'rate': 0, 'value': 6}, 'al': {'rate': 0, 'value': 3},
                              'zn': {'rate': 0, 'value': 3},
                              'au': {'rate': 0, 'value': 10}, 'MA': {'rate': 0, 'value': 2}, 'RM': {'rate': 0, 'value': 2},
                              'FG': {'rate': 0, 'value': 3},'v':{'rate':0,'value':2},
                              'ZC': {'rate': 0, 'value': 6}, 'CF': {'rate': 0, 'value': 6}, 'SR': {'rate': 0, 'value': 3},
                              'TA': {'rate': 0, 'value': 3},'IH':{'rate':0.32,'value':0},'IC':{'rate':0.32,'value':0},
                              'IF':{'rate':0.32,'value':0},'T':{'rate':0,'value':4.2},'TF':{'rate':0,'value':4.2}}

class Mutiple:
    def __init__(self):
         self._multiple = {'ru': 10, 'cu': 5, 'ag': 15, 'au': 1000, 'rb': 10, 'bu': 10, 'al': 5, 'zn': 5, 'pb': 5, 'ni': 1, 'sn': 1,
                            'j': 100, 'jm': 60, 'pvc': 5, 'p': 10, 'y': 10,'v':10,'sc':1000,
                            'c': 10, 'i': 100, 'jd': 10, 'b': 10, 'm': 10, 'l': 5, 'pp': 5, 'cs': 10, 'SR': 10, 'TA': 5, 'MA': 10,
                            'FG': 20, 'RM': 10, 'ZC': 100, 'CF': 5, 'SF': 5,'hc':10,'IH':300,'IC':200,'IF':300,'T':10000,'TF':10000}
    def getMutiple(self,symbol):
        return self._multiple[symbol]

class TickSize:
    def __init__(self):

         self._ticksize = {'ru': 5, 'cu': 10, 'ag': 1, 'au': 0.05, 'rb': 1, 'bu': 2, 'al': 5, 'zn': 5, 'pb': 5, 'ni': 10, 'sn': 10,
                            'j': 1, 'jm': 1, 'pvc': 5, 'p': 2, 'y': 2,'sc':0.1,
                            'c': 1, 'i': 0.5, 'jd': 1, 'b': 1, 'm': 1, 'l': 1, 'pp': 1, 'cs': 1, 'SR': 1, 'TA': 1, 'MA': 1, 'FG': 1,
                            'RM': 1, 'ZC': 0.2, 'CF': 5, 'SF': 2}
class Margin:
    def __init__(self):
         self._margin = {'ru': 0.15, 'cu': 0.14, 'ag': 0.13, 'au': 0.11, 'rb':0.14, 'bu':0.14, 'al':0.12, 'zn': 0.12, 'pb':0.12, 'ni': 0.13, 'sn': 0.12,
                          'j': 0.16, 'jm': 0.16, 'pvc': 0.11, 'p': 0.13, 'y':0.11,'sc':0.12,
                          'c': 0.11, 'i': 0.15, 'jd': 0.12, 'b': 0.11, 'm': 0.11, 'l': 0.11, 'pp': 0.11, 'cs': 0.11, 'SR': 0.09, 'TA': 0.12, 'MA':0.11,
                          'FG': 0.12, 'RM': 0.12,  'CF': 0.12,'ZC': 0.12, 'SF': 0.13}
