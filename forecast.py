# With help from the tutorial found at
# https://machinelearningmastery.com/time-series-prediction-lstm-recurrent-neural-networks-python-keras/

import os
import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, LSTM
from keras.optimizers import RMSprop
from sklearn.preprocessing import RobustScaler
from sklearn.metrics import mean_squared_error
from urllib import request

# fix random seed for reproducibility
np.random.seed(7)

price_col = 'adjusted_close'
features = [price_col, 'open', 'high', 'low', 'close', 'volume']
ticker = 'ORIG'

def getData(ticker, datasets_dirpath = 'datasets', outputsize = 'full', reverse = True):
    # outputsize == 'compact' gives only 100 rows of data (only the last 100 trading days)
    if outputsize == 0 or outputsize == 'compact':
        outputsize = 'compact'
    else:
        outputsize = 'full'
    fname = f'daily_adjusted_{ticker}.csv'
    fpath = os.path.join(datasets_dirpath, fname)
    if not os.path.isfile(fpath):
        print(f'Downloading {ticker} data from AlphaVantage ...', end = ' ')
        # Example CSV URL:
        # https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT&apikey=demo&datatype=csv
        apikey = 'ED5Z7Q9WDRQ29XST'
        # Download the CSV
        url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={ticker}&outputsize={outputsize}&apikey={apikey}&datatype=csv'
        request.urlretrieve(url, fpath)
        print('done.')
    print(f'Reading {ticker} data from {fpath} ...', end = ' ')
    df = pd.read_csv(fpath)
    if reverse:
        df = df.sort_values(by = 'timestamp', ascending = True).reset_index(drop = True)
    print('done.')
    return df

data = original_data = getData(ticker)
data = data[features].values.astype('float32')

def batchTransform(data, features):
    data = data.copy()
    scalers = {}
    for idx, feature in enumerate(features):
        scaler = RobustScaler()
        data[:, idx] = scaler.fit_transform(data[:, idx].reshape(-1, 1)).flatten()
        scalers[feature] = scaler
    return data, scalers

# Normalize dataset; RobustScaler is resistant to outliers
print('Scaling the data ... ', end = ' ')
data, scalers = batchTransform(data, features)
print('done.')

# Split into train and test sets
print(f'Splitting into train and test sets ...', end = ' ')
train_size = int(len(data) * 0.8)
val_size = int(len(data) * 0.1)
test_size = len(data) - train_size - val_size
train = data[0:train_size, :]
val = data[train_size:train_size + val_size, :]
test = data[train_size + val_size:len(data), :]
print('done.')

print(f'Train set length: {len(train)}, val set length: {len(val)}, test set length: {len(test)}')

def createDataset(data, lookback = 1):
    ''' Extracts the sample and target pairs '''
    dataX, dataY = None, None
    for i in range(len(data) - lookback - 1):
        a = data[i:(i + lookback), :]
        if dataX is None:
            dataX = a
        else:
            dataX = np.concatenate((dataX, a))
        # data[i + lookback, 0] only has one dimension, so we need to wrap it in np.array
        b = np.array([data[i + lookback, 0]])
        if dataY is None:
            dataY = b
        else:
            dataY = np.concatenate((dataY, b))
    # samples, targets
    return dataX, dataY

# Reshape into X = data@t and Y = data@t+1
lookback = 1
trainX, trainY = createDataset(train, lookback)
valX, valY = createDataset(val, lookback)
testX, testY = createDataset(test, lookback)

# We currently have 2-tensor: [samples, features]
# LSTM expects 3-tensor: [samples, time steps, features]
trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
valX = np.reshape(valX, (valX.shape[0], 1, valX.shape[1]))
testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))

# Params
neurons = 64
input_layers = 1
output_layers = 1
epochs = 15
batch_size = 5
verbose = 1

print('Building the model ...')
# Build the model architecture
model = Sequential()
model.add(LSTM(neurons,
               dropout = 0.1,
               recurrent_dropout = 0.5,
               input_shape = (input_layers, len(features)),
               return_sequences = True))
model.add(LSTM(neurons // 2,
               dropout = 0.5,
               return_sequences = False))
model.add(Dense(output_layers))
model.compile(loss = 'mse', optimizer = 'adam')
history = model.fit(trainX, trainY,
                    epochs = epochs, batch_size = batch_size,
                    validation_data = (valX, valY),
                    verbose = verbose)

# Make predictions
print('Predicting and inverse_transforming ...', end = ' ')
trainPredict = model.predict(trainX)
valPredict = model.predict(valX)
testPredict = model.predict(testX)

# Invert predictions
# trainPredict = scalers[price_col].inverse_transform(trainPredict)
# trainY = scalers[price_col].inverse_transform([trainY])
#
# valPredict = scalers[price_col].inverse_transform(valPredict)
# valY = scalers[price_col].inverse_transform([valY])
#
# testPredict = scalers[price_col].inverse_transform(testPredict)
# testY = scalers[price_col].inverse_transform([testY])

trainPredict = scalers[price_col].inverse_transform(trainPredict)
trainY = scalers[price_col].inverse_transform([trainY])

valPredict = scalers[price_col].inverse_transform(valPredict)
valY = scalers[price_col].inverse_transform([valY])

testPredict = scalers[price_col].inverse_transform(testPredict)
testY = scalers[price_col].inverse_transform([testY])
print('done.')

print(f'trainY:\n{trainY}\n')
print(f'trainPredict:\n{trainPredict}\n')
print(f'testY:\n{testY}\n')
print(f'testPredict:\n{testPredict}\n')

# Calculate loss with our loss function (mse)
print('Calculating scores (losses) ...', end = ' ')
trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:, 0]))
testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:, 0]))
print('done.')
print(f'Train score: {round(trainScore, 2)}')
print(f'Test score: {round(testScore, 2)}')

# Shift train predictions for plotting
trainPredictPlot = np.empty_like(data)
trainPredictPlot[:, :] = np.nan
trainPredictPlot[lookback:len(trainPredict) + lookback, :] = trainPredict
# Shift val predictions for plotting
valPredictPlot = np.empty_like(data)
valPredictPlot[:, :] = np.nan
valPredictPlot[len(trainPredict) + 2 * lookback + 1:len(trainPredict) + 2 * lookback + 1 + len(valPredict), :] = valPredict
# Shift test predictions for plotting
testPredictPlot = np.empty_like(data)
testPredictPlot[:, :] = np.nan
testPredictPlot[len(trainPredict) + 2 * lookback + 2 + len(valPredict):len(data) - 2, :] = testPredict
# Plot baseline and predictions
plt.plot(original_data[price_col], label = 'Real data')
plt.plot(trainPredictPlot[:, 0], label = 'Train prediction')
plt.plot(valPredictPlot[:, 0], label = 'Validation prediction')
plt.plot(testPredictPlot[:, 0], label = 'Test prediction')
plt.title(f'{ticker} training\n({epochs} epochs, {neurons}-neuron LSTM with 1 Dense, dropout, Adam, MAE, score = {round(testScore, 2)}')
plt.legend()
plt.show()


# Experiments to try:
# Add more recurrent layers
# Adjust the number of units in each recurrent layer
# Adjust the learning rate (hyperparam)
# Compare LSTM vs. GRU
# Add a stack of Dense layers on top of the RNN
# Finally, run the models with the best validation MAE on the test set