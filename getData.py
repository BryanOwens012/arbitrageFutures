import pandas as pd
import numpy as np
from urllib import request
import os
from datetime import datetime
import time

stocks = ['amzn', 'aapl', 'baba', 'nvda', 'abb', 'ge',
           'seas', 'bidu', 'axas', 'cane', 'hmny', 'sbux',
           'adbe', 'nok', 'fb', 'soyb', 'dia', 'csco', 'amd', 'spy',
           'cron', 'dnr', 'fdn', 'orig', 'chgg', 'nflx', 'crm', 'fas',
           'wwe', 'vixy', 'ita', 'trx', 'aytu', 'cpst', 'nvcn',
           'spot', 'aker', 'msft', 'goog', 'mu', 'dks', 'lmt', 'f',
           'twtr', 'tsla', 'xom', 'dal', 'gpro', 'baba', 'bac', 'fit', 'tal',
          'asdfasdfasdf']
cryptos = ['btc', 'bch', 'eth', 'ltc', 'neo']

def sortUpper(tickers):
    return sorted(set([ticker.upper() for ticker in tickers]))

stocks = sortUpper(stocks)
cryptos = sortUpper(cryptos)

class Timer():
    def __init__(self):
        self.start_time = datetime.now()
    def getTime(self):
        return datetime.now() - self.start_time
    def reset(self):
        self.__init__()

def getData(ticker, security = 'stock', datasets_dirpath = 'datasets', outputsize = 'full'):
    # outputsize == 'compact' gives only 100 rows of data (only the last 100 trading days)
    if outputsize == 0 or outputsize == 'compact':
        outputsize = 'compact'
    else:
        outputsize = 'full'
    fname = f'daily_adjusted_{ticker}.csv'
    fpath = os.path.join(datasets_dirpath, fname)
    if not os.path.isfile(fpath):
        try:
            print(timer.getTime(), end = '\t')
        except NameError:
            pass
        sleep_time = 5
        print(f'Downloading {ticker} --> {fpath} (sleeping {sleep_time}s first) ...', end = ' ')
        time.sleep(sleep_time)
        try:
            # Example CSV URL:
            # https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT&apikey=demo&datatype=csv
            apikey = 'ED5Z7Q9WDRQ29XST'
            # Download the CSV
            if security == 'stock':
                url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={ticker}&outputsize={outputsize}&apikey={apikey}&datatype=csv'
            if security == 'crypto' or security == 'cryptocurrency':
                url = f'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol={ticker}&market=USD&apikey={apikey}&datatype=csv'
            if not os.path.exists(datasets_dirpath):
                os.makedirs(datasets_dirpath, exist_ok = True)
            request.urlretrieve(url, fpath)
            # If the csv turns out to be an error message, retry
            df = pd.read_csv(fpath)
            if df.shape[0] < 5 or df.shape[1] < 1:
                print(f'\n\tDownload for {ticker} is invalid. Trying again ...', end = ' ')
                os.remove(fpath)
                time.sleep(sleep_time)
                request.urlretrieve(url, fpath)
                df = pd.read_csv(fpath)
                if df.shape[0] < 5 or df.shape[1] < 1:
                    print(f'\n\tDownload for {ticker} failed.\n\t\tPerhaps "{ticker}" is spelled incorrectly or you have exceeded your AlphaVantage API limits.\n\tTry again later.')
                    os.remove(fpath)
                    return
            print(f'done.')
        except (FileNotFoundError, NameError) as error:
            print(f'ERROR: could not download data for {ticker}.')
    else:
        try:
            print(timer.getTime(), end = '\t')
        except NameError:
            pass
        print(f'{ticker} data already downloaded.')

timer = Timer()
print(f'Stocks to download: {stocks}')
for stock in stocks:
    getData(stock)
print(f'Cryptos to download: {cryptos}')
for crypto in cryptos:
    getData(crypto, security = 'crypto')