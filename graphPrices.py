import pandas as pd
import numpy as np
import os
import datetime
from math import sqrt
import matplotlib.pyplot as plt

horizons = ['30s', '5T'] # s = seconds, T = minutes, H = hours, D = day
shifts = ['0s', '1T']

start_time_total = datetime.datetime.now().replace(microsecond = 0)
round_number = 0

root_dir = '' # relative path
# stock1_dir = 'dailyTick/000001'
# stock2_dir = 'dailyTick/000002'
stock1_dir = 'ZC809'
stock2_dir = 'ZC811'

# The following are the prefixes of the stocks.
# E.g., for ZC809_20180604.csv the prefix is ZC809
# By default the prefix is equal to the basename of the dirs,
# but user should change it depending on the naming scheme of the CSVs
stock1_prefix = os.path.basename(stock1_dir)
stock2_prefix = os.path.basename(stock2_dir)

out_dir = ''

# Make names into paths
stock1_dir_path = os.path.join(root_dir, stock1_dir)
stock2_dir_path = os.path.join(root_dir, stock2_dir)
out_dir_path = os.path.join(root_dir, out_dir)

# Make sure the specified folders exist
missing = [path for path in [stock1_dir_path, stock2_dir_path] if not os.path.isdir(path)]

if len(missing) > 0:
    print('Missing these folders:', missing)
    print('Terminating.')
    quit()

def listCSV(path):
    ''' A generator to list all CSVs in the path dir '''
    files = (os.path.join(path, file) for file in os.listdir(path) if os.path.isfile(os.path.join(path, file)) and os.path.join(path, file).endswith('.csv'))
    return files

def listDirs(path):
    ''' A generator to list all subdirs of the path dir '''
    dirs = (os.path.join(path, dir) for dir in os.listdir(path) if os.path.isdir(os.path.join(path, dir)))
    return dirs


class graphObject():
    def __init__(self, x, y1, y2, y_label, horizon, shift1 = '0s', shift2 = '0s'):
        self.x = pd.to_datetime(x)
        self.y1 = y1
        self.y2 = y2
        self.y_label = y_label
        self.horizon = horizon
        self.shift1 = shift1
        self.shift2 = shift2
    def graph(self):
        plt.plot_date(self.x, self.y1, 'g-', label = f'Stock1 (ZC809) (horizon = {self.horizon}, shift1 = {self.shift1})')
        plt.plot_date(self.x, self.y2, 'b-', label = f'Stock2 (ZC811) (horizon = {self.horizon}, shift2 = {self.shift2})')
        plt.xlabel('TimeStamp')
        plt.ylabel(self.y_label)
        plt.legend()
        plt.gcf().autofmt_xdate()
        plt.suptitle(f'Stock1 vs stock2 on the day beginning {self.x.iloc[0]}')
        plt.show()
        plt.clf()

class DFObject:
    def __init__(self, file = None, df = None):
        self.horizons = horizons
        self.shifts = shifts
        for shift in self.shifts.copy():
            None if shift in ['0', '0s'] else self.shifts.append('-' + shift)

        # Init structure for dynamically calculating correlation
        # Corr(x,y) = Cov(x, y) / sqrt(Var(x) * Var(y))
        # Population (Estimate) Cov(x, y) = E(XY) - E(X)E(Y) = sum(XY) / n - sum(X) * sum(Y) / n^2
        # Population (Estimate) Var(x) = E(X^2) - (E(X))^2 = sum(X^2) / n - (sum(X))^2 / n^2
        # n == x_count == y_count necessarily, since I dropna'd
        self.corr_data = {horizon:
            {shift1:
                {shift2:
                    {
                        'n': 0,
                        'x_sum': 0, 'y_sum': 0,
                        'xx_sum': 0, 'yy_sum': 0,
                        'xy_sum': 0, 'corr': 0}
                    for shift2 in self.shifts}
                for shift1 in self.shifts}
            for horizon in self.horizons}
        self.stock1 = pd.DataFrame()
        self.stock2 = pd.DataFrame()
        self.dates_done = []
        self.stock1_files = []
        self.stock2_files = []

    def addStock1Input(self, file = None, df = None):
        print('Adding {0} to {1} ...'.format(file, 'stock1'))
        if file == None:
            if df == None:
                print('Error: invalid {0} input filepath or df'.format('stock1'))
                return
            elif df != None:
                self.stock1 = pd.concat([self.stock1, df], ignore_index = True)
        elif file != None:
            if not os.path.isfile(file):
                self.stock1 = pd.DataFrame()
            elif os.path.isfile(file):
                self.stock1 = pd.concat([self.stock1, pd.read_csv(file)], ignore_index = True, sort = True)
                self.stock1_files.append(file)

    def addStock2Input(self, file = None, df = None):
        print('Adding {0} to {1} ...'.format(file, 'stock2'))
        if file == None:
            if df == None:
                print('Error: invalid {0} input filepath or df'.format('stock2'))
                return
            elif df != None:
                self.stock2 = pd.concat([self.stock2, df], ignore_index = True)
        elif file != None:
            if not os.path.isfile(file):
                self.stock2 = pd.DataFrame()
            elif os.path.isfile(file):
                self.stock2 = pd.concat([self.stock2, pd.read_csv(file)], ignore_index = True, sort = True)
                self.stock2_files.append(file)

    def resetStocks(self):
        print('Resetting self.stock1 and self.stock2 ...')
        self.stock1 = pd.DataFrame()
        self.stock2 = pd.DataFrame()

    def addOneDateInput(self, date_str):
        print('Adding date {0} ...'.format(date_str))
        file1 = os.path.join(stock1_dir_path, os.path.basename(stock1_prefix) + '_' + date_str + '.csv')
        file2 = os.path.join(stock2_dir_path, os.path.basename(stock2_prefix) + '_' + date_str + '.csv')
        self.addStock1Input(file1)
        self.addStock2Input(file2)

    def doCorrsOneDate(self, date):
        self.stock_cols = ['TimeStamp', 'InstrumentID', 'LastPrice']

        def _clean(stock):
            stock = stock.loc[(stock['LastPrice'] > 0) & (stock['LastPrice'] != np.nan), self.stock_cols]
            stock['TimeStamp'] = pd.to_datetime(stock['TimeStamp'], unit = 'ms')
            stock.sort_values(by = 'TimeStamp')
            stock.reset_index(inplace = True, drop = True) # @param drop = True to drop the old index
            return stock

        print('Cleaning the DFs ...')
        self.stock1 = _clean(self.stock1)
        self.stock2 = _clean(self.stock2)

        # Skip the file if the LastPrice column in either file is empty, all NaN, or all -1
        if self.stock1.shape[0] == 0 or self.stock2.shape[0] == 0:
            print('The stocks for {0} are invalid; skipping ...'.format(date))
            return

        def _resample(df, horizon, shift):
            resampled_stock = df.set_index('TimeStamp').shift(freq = shift).loc[:, 'LastPrice'].resample(horizon, label = 'left').ohlc()
            resampled_stock.fillna(method = 'ffill', inplace = True)
            resampled_stock.reset_index(inplace = True)
            return resampled_stock

        def _merge(stock1, stock2):
            return stock1.merge(stock2, on = 'TimeStamp')

        def _writeCorrsOne(horizon, shift1, shift2):
            # Resample stock1 and stock2 by the same horizon, then merge them to align timestamps
            merged = _merge(_resample(self.stock1, horizon, shift1), _resample(self.stock2, horizon, shift2))
            merged.reset_index(inplace = True)

            # Alias for the desired corr_data container
            d = self.corr_data[horizon][shift1][shift2]

            # Calculate returns
            col1_name = 'returns_x'
            col2_name = 'returns_y'
            merged[col1_name] = (merged['close_x'] - merged['open_x']) / merged['open_x']
            merged[col2_name] = (merged['close_y'] - merged['open_y']) / merged['open_y']
            # print(merged) # print the binned prices, for debugging

            # These are UTC times; China is UTC+8, so UTC is China-8
            # Trading sessions: 9:30-11:30 => 1:30-3:30, 13:00-15:00 => 5:00-7:00
            merged_session1 = merged.set_index('TimeStamp').between_time('1:30', '3:30')
            merged_session2 = merged.set_index('TimeStamp').between_time('5:00', '7:00')
            merged = pd.concat([merged_session1, merged_session2]).reset_index()

            self.printPeaks(merged)

            d['n'] += merged.shape[0]
            if d['n'] == 0 or merged.shape[0] == 0 or merged.shape[1] == 0:
                print('{0} (Horizon = {1}, shift1 = {2}, shift2 = {3} has invalid data or misaligned timestamps; skipping ...'.format(
                    date, horizon, shift1, shift2))
                return
            d['x_sum'] += merged[col1_name].sum()
            d['y_sum'] += merged[col2_name].sum()
            d['xx_sum'] += (merged[col1_name] ** 2).sum()
            d['yy_sum'] += (merged[col2_name] ** 2).sum()
            d['xy_sum'] += (merged[col1_name] * merged[col2_name]).sum()
            cov = d['xy_sum'] / d['n'] - d['x_sum'] * d['y_sum'] / d['n'] ** 2
            x_var = d['xx_sum'] / d['n'] - d['x_sum'] ** 2 / d['n'] ** 2
            y_var = d['yy_sum'] / d['n'] - d['y_sum'] ** 2 / d['n'] ** 2
            if x_var == 0 or y_var == 0:
                corr = d['corr'] = 0
            else:
                corr = d['corr'] = cov / sqrt(x_var * y_var)

            print('{0}\t{1}\thorizon = {2}, shift1 = {3}, shift2 = {4}:\tcum_corr = {5}'.format(datetime.datetime.now() - start_time_total, date, horizon, shift1, shift2, corr))

            # Use returns_x  and returns_y for returns
            # Use close_x and close_y for prices
            graph = graphObject(merged['TimeStamp'], merged['close_x'], merged['close_y'], 'Prices', horizon, shift1, shift2)
            graph.graph()

        for horizon in horizons:
            for shift1 in shifts:
                for shift2 in shifts:
                    _writeCorrsOne(horizon, shift1, shift2)

    def printCorrs(self, corr_data):
        best_corr = {'horizon': 0, 'shift1': 0, 'shift2': 0, 'n': 0, 'corr': 0}
        for horizon in corr_data:
            horizon_name = horizon
            horizon = corr_data[horizon]
            print('Horizon = {0}'.format(horizon_name))
            for shift1 in horizon:
                shift1_name = shift1
                shift1 = horizon[shift1]
                print('\tShift1 = {0}'.format(shift1_name))
                for shift2 in shift1:
                    shift2_name = shift2
                    shift2 = shift1[shift2]
                    if abs(shift2['corr']) > abs(best_corr['corr']) and shift2['corr'] != 1:
                        best_corr = {'horizon': horizon_name, 'shift1': shift1_name, 'shift2': shift2_name, 'n': shift2['n'], 'corr': shift2['corr']}
                    print('\t\tHorizon = {0}, shift1 = {1}, shift2 = {2}, n = {3}, corr = {4}'.format(
                        horizon_name, shift1_name, shift2_name, shift2['n'], shift2['corr']))
        print('Best overall corr = {0}'.format(best_corr))

    def printPeaks(self, df):
        returns_x_threshold = 0.001
        returns_y_threshold = 0.001
        print(f'Sudden jumps in Stock1 (abs(jump) >= {returns_x_threshold}):\n', df.loc[abs(df['returns_x']) >= returns_x_threshold])
        print(f'Sudden jumps in Stock2 (abs(jump) >= {returns_y_threshold}): \n', df.loc[abs(df['returns_y']) >= returns_y_threshold])

    def doCorrsAll(self):
        stock1_set = set(os.path.splitext(os.path.basename(file))[0].split('_')[1] for file in listCSV(stock1_dir_path))
        stock2_set = set(os.path.splitext(os.path.basename(file))[0].split('_')[1] for file in listCSV(stock2_dir_path))
        shared_dates = sorted(list(stock1_set.intersection(stock2_set)), reverse = False)
        print('Found these {0} dates to evaluate:\n{1}'.format(len(shared_dates), shared_dates))

        # shared_dates = ['20171012', '20171017', '20171018'] # for testing
        for date in shared_dates:
            print('\n ====================== \n {0} \t Evaluating date {1} ... '.format(
                datetime.datetime.now() - start_time_total, date))
            self.addOneDateInput(date)
            self.doCorrsOneDate(date)
            self.dates_done.append(date)
            self.resetStocks()
        self.printCorrs(self.corr_data)



df = DFObject()
df.doCorrsAll()
