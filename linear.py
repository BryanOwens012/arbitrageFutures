# With guidance from: https://enlight.nyc/stock-market-prediction/
# Data from AlphaVantage: https://www.alphavantage.co/documentation/

import pandas as pd
import numpy as np
import datetime
import os
import quandl
from urllib import request
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
from sklearn import preprocessing, model_selection

# Get stock data
# Note that stock splits are not reflected
ticker = 'TSLA'

def getData(ticker, datasets_dirpath = 'datasets', outputsize = 'full', reverse = True):
    # outputsize == 'compact' gives only 100 rows of data (only the last 100 trading days)
    if outputsize == 0 or outputsize == 'compact':
        outputsize = 'compact'
    else:
        outputsize = 'full'
    fname = f'daily_adjusted_{ticker}.csv'
    fpath = os.path.join(datasets_dirpath, fname)
    if not os.path.isfile(fpath):
        print(f'Downloading {ticker} data from AlphaVantage ...', end = ' ')
        # Example CSV URL:
        # https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT&apikey=demo&datatype=csv
        apikey = 'ED5Z7Q9WDRQ29XST'
        # Download the CSV
        url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={ticker}&outputsize={outputsize}&apikey={apikey}&datatype=csv'
        request.urlretrieve(url, fpath)
        print('done.')
    print(f'Reading {ticker} data from {fpath} ...', end = ' ')
    df = pd.read_csv(fpath)
    if reverse:
        df = df.sort_values(by = 'timestamp', ascending = True).reset_index(drop = True)
    print('done.')
    return df

price_col = 'adjusted_close'
forecast_out = int(30) # predicting 30 days into the future
df = df_original = getData(ticker)[:-forecast_out]

def plotPrices(df, ticker):
    print(f'\n\n{ticker} data:')
    print(df)
    print('\n\n')

    # Plot price over time
    print(f'Plotting stock data for {ticker} ...', end = ' ')
    plt.plot(df.iloc[:, 0], label = 'Price')
    plt.title(f'{ticker}')
    plt.xlabel('Date')
    plt.ylabel('Price ($)')
    print('done.')
    plt.show()

df = df[[price_col]]
df['Prediction'] = df[[price_col]].shift(-forecast_out)

# Preprocessing
X = np.array(df.drop(['Prediction'], axis = 1))
X = preprocessing.scale(X)
X_forecast = X[-forecast_out:] # set X_forecast equal to last 30
X = X[:-forecast_out]
y = np.array(df['Prediction'])
y = y[:-forecast_out]

# Regression
test_size = 0.2
X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y,
                                                                    test_size = test_size)

# Training
linear = LinearRegression()
linear.fit(X_train, y_train)

# Testing
confidence = linear.score(X_test, y_test)
print(f'Confidence: {confidence}')

# Forecast
forecast_prediction = linear.predict(X_forecast)
print(f'forecast_prediction:\n{forecast_prediction}')

def plotForecast():
    # Print forecasted prices with real prices
    historical_plot = df_original[[price_col]]
    tail_plot = df_original.iloc[-forecast_out:, df_original.columns.get_loc(price_col)]
    forecast_prediction_plot = pd.DataFrame(data = forecast_prediction, index = tail_plot.index)
    plt.plot(historical_plot, label = 'Historical price')
    plt.plot(tail_plot, label = 'Actual price')
    plt.plot(forecast_prediction_plot, label = 'Predicted price')
    plt.title(f'Predicted ${ticker} price for {forecast_out} days out ({round(confidence * 100, 1)}% confidence)')
    plt.legend()
    plt.show()

plotForecast()

def a():
    X_forecast_now = 3
    # forecast_now = linear.predict(np.array(getData(ticker, 'datasets')[-forecast_out:, ])
    print(linear.predict())
    # historical_