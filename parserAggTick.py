import os
import datetime
import pandas as pd

dirpath = os.path.join('hide', 'aggTick')
stocks = ['ZC809', 'ZC811']

os.chdir(dirpath)
fnames = [fname for fname in os.listdir('.')
          if os.path.isfile(fname) and fname.endswith('.csv')]
print(f'Found these files: {fnames}')

class Timer:
    def __init__(self):
        self._begin_time = datetime.datetime.now()

    def getTime(self):
        return datetime.datetime.now() - self._begin_time

    def reset(self):
        self.__init__()

def extractOneCSV(fname):
    print(f'{timer.getTime()}\tProcessing {fname} ...')
    df_in = pd.read_csv(fname)
    for stock in stocks:
        df_out = df_in.loc[df_in['InstrumentID'] == stock]
        # Concat the stock name, an underscore, and the date
        fname_out = stock + '_' + fname.split('_')[-1]
        df_out.to_csv(fname_out, index = False)
        print(f'\tFinished writing {fname_out}.')

timer = Timer()
for fname in fnames:
    extractOneCSV(fname)
