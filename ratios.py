# Originally created for ZC 1809 and ZC 1811,
# but should work for arbitrage for all futures contracts pairs

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import os
import json
import datetime

# Read params from params file
params_path = 'ratios_params.json'
with open(params_path, 'r') as json_data:
    params = json.load(json_data)

start_time_total = datetime.datetime.now().replace(microsecond = 0)
round_number = 0

stock1_name = params['stock1_name']
stock2_name = params['stock2_name']

# The stock1_prefix and stock2_prefix are the prefixes of the stocks.
# E.g., stock1_name = ZC809, stock1_dir = ZC809, stock1_prefix = ZC809 for the file ZC809_20180604.csv
# The following are the defaults,
# but you should change them here depending on the naming scheme of the CSVs
stock1_dir = stock1_name
stock2_dir = stock2_name
# The prefixes are used only for adding date inputs into a DFObject
stock1_prefix = os.path.basename(stock1_dir)
stock2_prefix = os.path.basename(stock2_dir)

# Make names into paths
stock1_dir_path = os.path.join(params['root_dir'], stock1_dir)
stock2_dir_path = os.path.join(params['root_dir'], stock2_dir)

# Make sure the specified folders exist
missing = [path for path in [stock1_dir_path, stock2_dir_path] if not os.path.isdir(path)]

if len(missing) > 0:
    print('Missing these folders:', missing)
    print('Terminating.')
    quit()

def listCSV(path):
    ''' A generator to list all CSVs in the path dir '''
    files = (os.path.join(path, file) for file in os.listdir(path) if os.path.isfile(os.path.join(path, file)) and os.path.join(path, file).endswith('.csv'))
    return files

def listDirs(path):
    ''' A generator to list all subdirs of the path dir '''
    dirs = (os.path.join(path, dir) for dir in os.listdir(path) if os.path.isdir(os.path.join(path, dir)))
    return dirs

class DFObject:
    def __init__(self, file = None, df = None):
        self.profits = {horizon: [] for horizon in params['horizons']}
        self.stock1 = pd.DataFrame()
        self.stock2 = pd.DataFrame()
        self.dates_done = []
        self.stock1_files = []
        self.stock2_files = []
        self.stock_cols = ['TimeStamp', 'InstrumentID', 'LastPrice', 'AskPrice1', 'BidPrice1']


    def addStock1Input(self, file = None, df = None):
        print('Adding {0} to {1} ...'.format(file, 'stock1'))
        if file == None:
            if df == None:
                print('Error: invalid {0} input filepath or df'.format('stock1'))
                return
            elif df != None:
                self.stock1 = pd.concat([self.stock1, df], ignore_index = True)
        elif file != None:
            if not os.path.isfile(file):
                self.stock1 = pd.DataFrame()
            elif os.path.isfile(file):
                self.stock1 = pd.concat([self.stock1, pd.read_csv(file)], ignore_index = True, sort = True)
                self.stock1_files.append(file)

    def addStock2Input(self, file = None, df = None):
        print('Adding {0} to {1} ...'.format(file, 'stock2'))
        if file == None:
            if df == None:
                print('Error: invalid {0} input filepath or df'.format('stock2'))
                return
            elif df != None:
                self.stock2 = pd.concat([self.stock2, df], ignore_index = True)
        elif file != None:
            if not os.path.isfile(file):
                self.stock2 = pd.DataFrame()
            elif os.path.isfile(file):
                self.stock2 = pd.concat([self.stock2, pd.read_csv(file)], ignore_index = True, sort = True)
                self.stock2_files.append(file)

    def resetStocks(self):
        print('Resetting self.stock1 and self.stock2 ...')
        self.stock1 = pd.DataFrame()
        self.stock2 = pd.DataFrame()

    def addOneDateInput(self, date_str):
        print('Adding date {0} ...'.format(date_str))
        file1 = os.path.join(stock1_dir_path, os.path.basename(stock1_prefix) + '_' + date_str + '.csv')
        file2 = os.path.join(stock2_dir_path, os.path.basename(stock2_prefix) + '_' + date_str + '.csv')
        self.addStock1Input(file1)
        self.addStock2Input(file2)

    def printProfits(self):
        best_profit = {'horizon': None, 'total_profit': 0}
        print('\nProfit for each horizon:')
        for horizon in self.profits:
            total_profit = sum(self.profits[horizon])
            if best_profit['horizon'] == None or total_profit > best_profit['total_profit']:
                best_profit = {'horizon': horizon, 'total_profit': total_profit}
            print(f'\tHorizon = {horizon}, total_profit = {total_profit} (avg. ${round(total_profit / len(self.dates_done), 2)}/day)')
        print(f'Best profit: horizon = {best_profit["horizon"]}, total_profit = {round(best_profit["total_profit"], 2)} (avg. ${round(best_profit["total_profit"] / len(self.dates_done), 2)}/day)')

    def doRatiosOneDate(self, date):

        def _clean(stock):
            stock = stock.loc[(stock['LastPrice'] > 0) & (stock['LastPrice'] != np.nan), self.stock_cols]
            stock['TimeStamp'] = pd.to_datetime(stock['TimeStamp'], unit = 'ms')
            stock.sort_values(by = 'TimeStamp')
            stock.reset_index(inplace = True, drop = True) # @param drop = True to drop the old index
            return stock

        print('Cleaning the DFs ...')
        self.stock1 = _clean(self.stock1)
        self.stock2 = _clean(self.stock2)

        # Skip the file if the LastPrice column in either file is empty, all NaN, or all -1
        if self.stock1.shape[0] == 0 or self.stock2.shape[0] == 0:
            print('The stocks for {0} are invalid; skipping ...'.format(date))
            return

        def _resample(stock, horizon):
            resampled_stock = stock.sort_values(by = 'TimeStamp').set_index('TimeStamp').loc[:, ['LastPrice', 'AskPrice1', 'BidPrice1']].resample(horizon, label = 'left').ohlc()
            resampled_stock.fillna(method = 'ffill', inplace = True)
            resampled_stock.reset_index(inplace = True)
            return resampled_stock

        def _merge(stock1, stock2):
            return stock1.merge(stock2, on = 'TimeStamp')

        def _betweenTimes(stock, time_pairs):
            stock = stock.set_index('TimeStamp')
            concat_stocks = []
            for pair in time_pairs:
                concat_stocks.append(stock.between_time(pair[0], pair[1]))
            stock = pd.concat(concat_stocks).reset_index()
            return stock

        # Simulation 1: buy when diff < mean rolling diff, sell when diff > mean rolling diff
        def _sim1(merged, horizon, original_bank = params['original_bank'], original_owned_x = params['original_owned_x'], original_owned_y = params['original_owned_y'], lot_size = params['lot_size'], multiplier = params['multiplier'], commission_factor = params['commission_factor'], commission_fixed = params['commission_fixed']):
            print(
                f'\nBacktesting (simulating) model for horizon = {horizon}, ...')
            print(f'(Original bank = ${original_bank}, original_owned_x = {original_owned_x} lots of stock1, original_owned_y = {original_owned_y} lots of stock2)')
            print(f'(Lot size = {lot_size} shares/lot, multiplier = {multiplier}, commission_factor = {commission_factor * 100}%, commission_fixed = ${commission_fixed}/lot)')
            bank = original_bank  # amount available to buy shares
            owned_x = original_owned_x
            owned_y = original_owned_y
            original_value = original_bank + owned_x * lot_size * multiplier * merged.iloc[0]['LastPrice_x']['open'] \
                                           + owned_y * lot_size * multiplier * merged.iloc[0]['LastPrice_y']['open']
            COMMISSION_FACTOR = commission_factor  # commission_factor is % of the trade price

            for idx, row in merged.iterrows():
                ratio = row['ratio'][0]
                ratio_rolling = row['ratio_rolling'][0]
                transact_amount_x = transact_amount_y = 1
                # Buy Stock1 and sell Stock2
                # if ratio < ratio_rolling and ratio / ratio_rolling <= below_max and owned_y >= transact_amount_y:
                if ratio < ratio_rolling and ratio / ratio_rolling <= params['max_below'] and owned_y >= transact_amount_y:
                    cost = transact_amount_y * (lot_size * multiplier * row['AskPrice1_y'][0] * (1 + COMMISSION_FACTOR) + commission_fixed)
                    revenue = transact_amount_y * (lot_size * multiplier * row['BidPrice1_x'][0] * (1 - COMMISSION_FACTOR) - commission_fixed)
                    if cost < bank and revenue > cost:
                        bank += revenue - cost
                        owned_x += transact_amount_y
                        owned_y -= transact_amount_y
                        print(
                            f'\tBought {transact_amount_y} lots x stock1 and sold {transact_amount_y} lots x stock2 for ${(revenue - cost)} profit')
                # Sell Stock1 and buy Stock2
                # elif ratio > ratio_rolling and ratio / ratio_rolling >= above_min and owned_x >= transact_amount_x:
                elif ratio > ratio_rolling and ratio / ratio_rolling >= params['min_above'] and owned_x >= transact_amount_x:
                    cost = transact_amount_x * (lot_size * multiplier * row['AskPrice1_x'][0] * (1 + COMMISSION_FACTOR) + commission_fixed)
                    revenue = transact_amount_x * (lot_size * multiplier * row['BidPrice1_y'][0] * (1 - COMMISSION_FACTOR) - commission_fixed)
                    if cost < bank and revenue > cost:
                        bank += revenue - cost
                        owned_x -= transact_amount_x
                        owned_y += transact_amount_x
                        print(
                            f'\tSold {transact_amount_x} lot{"" if transact_amount_x == 1 else "s"} x stock1 and bought {transact_amount_x} lot{"" if transact_amount_x == 1 else "s"} x stock2 for ${(revenue - cost)} profit')

            close_x = merged.iloc[-1]['LastPrice_x']['close']
            close_y = merged.iloc[-1]['LastPrice_y']['close']
            print(
                f'End of day bank: ${round(bank, 2)}, shares owned: {lot_size} * {owned_x} = {lot_size * owned_x} shares @ ${round(close_x, 2)}/share, {lot_size} * {owned_y} = {lot_size * owned_y} shares @ ${round(close_y, 2)}/share')

            # Buy and sell to reset number of shares to equal day open

            print('Buying and selling to reset shares to day open ...')

            # sell owned_x until we reach original_owned_x
            stock_profit = 0
            if owned_x > original_owned_x:
                diff = owned_x - original_owned_x
                print(f'Selling {diff} lots of Stock1 and buying {diff} lots of Stock2 ...')
                cost = diff * (lot_size * multiplier * merged.iloc[-1]['AskPrice1_y']['close'] * (1 + COMMISSION_FACTOR) + commission_fixed)
                revenue = diff * (lot_size * multiplier * merged.iloc[-1]['BidPrice1_x']['close'] * (1 - COMMISSION_FACTOR) - commission_fixed)
                stock_profit += revenue - cost
                owned_x -= diff
                owned_y += diff
            elif owned_x < original_owned_x:
                diff = original_owned_x - owned_x
                print(f'Buying {diff} Stock1 and selling {diff} Stock2 ...')
                cost = diff * (lot_size * multiplier * merged.iloc[-1]['AskPrice1_x']['close'] * (1 + COMMISSION_FACTOR) + commission_fixed)
                revenue = diff * (lot_size * multiplier * merged.iloc[-1]['BidPrice1_y']['close'] * (1 - COMMISSION_FACTOR) - commission_fixed)
                stock_profit += revenue - cost
                owned_x += diff
                owned_y -= diff

            assert (owned_x == original_owned_x and owned_y == original_owned_y)
            profit = (bank + stock_profit) - original_bank
            self.profits[horizon].append(profit)
            profit = round(profit, 2)  # round the dollars to 2 decimal places
            print(f'Profit: ${profit}')

        def _plotRatios(merged, horizon, rolling_amt):
            merged.plot(x = 'TimeStamp', y = ['ratio', 'ratio_rolling'],
                        kind = 'line',
                        title = f'Ratio of ZC809 vs ZC811, with horizon {horizon}, rolling_num = {rolling_amt}')
            plt.show()

        def _addReturnsCols(merged):
            merged = merged.copy()
            col1_name = 'returns_x'
            col2_name = 'returns_y'
            merged[col1_name] = (merged['LastPrice_x']['close'] - merged['LastPrice_x']['open']) / merged['LastPrice_x']['open']
            merged[col2_name] = (merged['LastPrice_y']['close'] - merged['LastPrice_y']['open']) / merged['LastPrice_y']['open']
            return merged

        def _doRatiosOne(horizon = '10s'):
            # Resample stock1 and stock2 by the same horizon, then merge them to align timestamps
            merged = _merge(_resample(self.stock1, horizon), _resample(self.stock2, horizon))
            merged.reset_index(inplace = True)

            # These are UTC times; China is UTC+8, so UTC is China-8
            # Trading sessions: 9:30-11:30 => 1:30-3:30, 13:00-15:00 => 5:00-7:00
            # So we only get the transactions within those time frames
            merged = _betweenTimes(merged, [['1:30', '3:30'], ['5:00', '7:00']])

            # Skip if the frame continues no prices
            if merged.shape[0] == 0 or merged.shape[1] == 0:
                print(f'{date} (Horizon = {horizon} has invalid data or misaligned timestamps; skipping ...')
                return

            # Calculate returns
            _addReturnsCols(merged)

            # Calculate the ratio of the LastPrices
            merged['ratio'] = merged['LastPrice_x']['close'] / merged['LastPrice_y']['close']
            merged['ratio_rolling'] = merged['ratio'].rolling(params['rolling_amt']).mean() # rolling rows
            # print(merged) # print the binned prices, for debugging

            _sim1(merged, horizon)
            # _plotRatios(merged, horizon, params['rolling_amt'])

        for horizon in params['horizons']:
                _doRatiosOne(horizon)

    def doRatiosAll(self):
        stock1_set = set(os.path.splitext(os.path.basename(file))[0].split('_')[1] for file in listCSV(stock1_dir_path))
        stock2_set = set(os.path.splitext(os.path.basename(file))[0].split('_')[1] for file in listCSV(stock2_dir_path))
        shared_dates = sorted(list(stock1_set.intersection(stock2_set)), reverse = False)
        print('Found these {0} dates to evaluate:\n{1}'.format(len(shared_dates), shared_dates))

        # shared_dates = ['20171012', '20171017', '20171018'] # for testing
        for date in shared_dates:
            print('\n ====================== \n {0} \t Evaluating date {1} ... '.format(
                datetime.datetime.now() - start_time_total, date))
            self.addOneDateInput(date)
            self.doRatiosOneDate(date)
            self.dates_done.append(date)
            self.resetStocks()
        self.printProfits()
        print(f'\nDates done: {self.dates_done}')

df = DFObject()
df.doRatiosAll()
