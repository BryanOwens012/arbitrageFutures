import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import datetime
import json

# params:
params_path = 'reversalPoints_params.json'
with open(params_path, 'r') as json_data:
    params = json.load(json_data)
try:
    tick_size = params['tick_size']
    tick_cutoff = params['tick_cutoff'] # minimum number of ticks difference to establish a trend
    dirname = params['dirname']
    fname = params['fname']
    tick_decimal_places = params['tick_decimal_places']
    price_decimal_places = params['price_decimal_places']
except KeyError:
    print('ERROR: missing a param')
path = os.path.join(dirname, fname)

# Assert preconditions
assert (os.path.isfile(path)), f'ERROR: {path} does not exist'
assert (tick_size > 0), f'ERROR: tick size must be greater than 0'

# Read df from csv
df = pd.read_csv(os.path.join(dirname, fname))

def betweenTimes(stock, time_pairs):
    stock = stock.set_index('TimeStamp')
    concat_stocks = []
    for pair in time_pairs:
        concat_stocks.append(stock.between_time(pair[0], pair[1]))
    stock = pd.concat(concat_stocks).reset_index()
    return stock

def clean(df):
    df = df.copy()
    df['TimeStamp'] = pd.to_datetime(df['TimeStamp'], unit = 'ms')
    df = df.sort_values(by = 'TimeStamp')
    df = df.loc[df['LastPrice'] > 0]
    # Only look at the times when the market was open
    df = betweenTimes(df, [['1:30', '3:30'], ['5:00', '7:00']])
    df = df.reset_index(drop = True)
    return df

def addMidPrice(df):
    print(f'Adding column MidPrice ...')
    df = df.copy()
    df['MidPrice'] = (df['AskPrice1'] + df['BidPrice1']) / 2
    return df

class Timer:
    def __init__(self):
        self._begin_time = datetime.datetime.now()

    def getTime(self):
        return datetime.datetime.now() - self._begin_time

    def reset(self):
        self.__init__()

# Main function
def getReversalPoints(df):
    print('Calculating and writing reversal points ...')
    # Start the timer
    timer = Timer()
    df = df.copy()
    trend_direction = None # 0 == constant, <0 == dropping, >0 == rising; in terms of price, not ticks
    direction_tick_col = 'TrendDirection_Tick' # in terms of ticks
    direction_price_col = 'TrendDirection_Price' # in terms of price
    if 'MidPrice' not in df.columns:
        print('MidPrice column is required but does not exist. Building it for you ...')
        df = addMidPrice(df)
    print(f'Iterating through {df.shape[0]} rows ...')
    for idx, row in df.iterrows():
        if idx % 1000 == 0 or idx == df.shape[0] - 1:
            print(f'\t{timer.getTime()}\tOn row {idx} ...')
        if idx == 0:
            trend_begin_time = df.loc[idx, 'TimeStamp']
            trend_direction = np.nan
            df.loc[idx, direction_price_col] = np.nan
            df.loc[idx, direction_tick_col] = np.nan
        else:
            diff = row['MidPrice'] - df.loc[idx - 1, 'MidPrice']
            if np.sign(diff) == np.sign(trend_direction) or np.sign(diff) == 0:
                trend_direction += diff
            elif np.sign(diff) != np.sign(trend_direction):
                if abs(trend_direction) >= abs(tick_cutoff * tick_size):
                    df.loc[idx - 1, direction_price_col] = round(trend_direction,
                                                                 price_decimal_places)
                    df.loc[idx - 1, direction_tick_col] = round(trend_direction / tick_size,
                                                                tick_decimal_places)
                    df.loc[idx - 1, direction_price_col] = round(df.loc[idx - 1, direction_price_col],
                                                                 price_decimal_places)
                    df.loc[idx - 1, 'TrendDuration'] = df.loc[idx - 1, 'TimeStamp'] - trend_begin_time
                trend_direction = diff
                trend_begin_time = row['TimeStamp']
    print('Done.')
    return df

def writeToCSV(df, fname, suffix):
    fname = os.path.splitext(fname)[0] + suffix
    print(f'Writing {df.shape[0]} rows to {fname} ...')
    df.to_csv(fname, index = False)

df = clean(df)
df = getReversalPoints(df)
writeToCSV(df, fname, '_withTrends.csv')
writeToCSV(df.dropna(), fname, '_trendsOnly.csv')

# df.dropna().plot(x = 'TimeStamp', y = 'MidPrice', legend = True)